# coding=utf-8

from flask import Flask, jsonify, request
from flask_restful import Api
from flask_cors import CORS
from flask_jwt_extended import JWTManager, jwt_required, get_jwt_identity

from os import system, forkpty, kill
from subprocess import Popen, call
from signal import SIGTERM

from .readConfigurationsFile import loadConfigurations
from .entities.entity import Session, engine, Base

from datetime import datetime
from time import time

from .entities.RevokedTokenModel import RevokedTokenModel
from .entities.Camera import Camera, CameraSchema
from src.entities.AccessLevel import AccessLevel, AccessLevelSchema
from src.entities.Administrator import Administrator, AdministratorSchema
from src.entities.Camera import Camera, CameraSchema
from src.entities.CamerasAdmin import CamerasAdmin, CamerasAdminSchema
from src.entities.EntriesRegistry import EntriesRegistry, EntriesRegistrySchema
from src.entities.Schedule import Schedule, ScheduleSchema
from src.entities.StateMachine import StateMachine, StateMachineSchema
from .resources.resourcesJWT import (UserLogin, UserLogoutAccess, UserLogoutRefresh, TokenRefresh, SecretResource)



# creating the Flask application
app = Flask(__name__)
# cors = CORS(app, resources={r"/*": {"origins": "*"}})
CORS(app)
api = Api(app)

# if needed, generate database schema
Base.metadata.create_all(engine)

configurations = {}
loadConfigurations('settings.conf', configurations)

#resources to login and logout
app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
jwt = JWTManager(app)

app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    session = Session()
    query = session.query(RevokedTokenModel).filter_by(jti=jti).first()
    session.close()
    return bool(query)


api.add_resource(UserLogin, '/login')
api.add_resource(UserLogoutAccess, '/logout/access')
api.add_resource(UserLogoutRefresh, '/logout/refresh')
api.add_resource(TokenRefresh, '/token/refresh')
api.add_resource(SecretResource, '/secret')


@app.route('/camera/<id>', methods=['GET'])
@jwt_required
def get_camera(id):
    # fetching from the database
    session = Session()
    current_user = get_jwt_identity()
    print(current_user)
    camara_admin_object = session.query(CamerasAdmin).filter_by(id_admin=current_user, id_camera=id).first()
    if camara_admin_object:
        schema= CameraSchema(many=False)
        camera_object = session.query(Camera).get(camara_admin_object.id_camera)
        # transforming into JSON-serializable objects
        camera = schema.dump(camera_object)
        # serializing as JSON
        session.close()
        return jsonify(camera.data), 200
    else:
        return jsonify({ 'error':'camera not found for user {}'.format(current_user)}), 204



@app.route('/admin/camera', methods=['GET'])
@jwt_required
def get_cameras_for_admin():
    # fetching from the database
    session = Session()
    current_user = get_jwt_identity()
    print(current_user)
    camara_admin_objects = session.query(CamerasAdmin).filter_by(id_admin=current_user).all()
    schema= CamerasAdminSchema(many=True)
    # transforming into JSON-serializable objects
    cameras = schema.dump(camara_admin_objects)
    # serializing as JSON
    session.close()
    if len(camara_admin_objects) == 0:
        return jsonify(cameras.data), 204
    else:
        return jsonify(cameras.data), 200


@app.route('/admin/camera/<id>', methods=['GET'])
@jwt_required
def get_camera_admin_by_id(id):
    # fetching from the database
    session = Session()
    current_user = get_jwt_identity()
    print(current_user)
    camara_admin_object = session.query(CamerasAdmin).filter_by(id_admin=current_user, id_camera=id).first()
    if camara_admin_object:
        schema= CamerasAdminSchema(many=False)
        # transforming into JSON-serializable objects
        camera = schema.dump(camara_admin_object)
        # serializing as JSON
        session.close()
        return jsonify(camera.data), 200
    else:
        return jsonify({ 'error':'camera not found for user {}'.format(current_user)}), 204


@app.route('/admin/camera', methods=['POST'])
@jwt_required
def add_camera():
    # mount CameraAdmin object
    #TODO añadir el cifrado de la contraseña de la camara
    posted_json=request.get_json()
    # posted_json['id']=posted_json['id_camera']
    # print(posted_json)

    if posted_json['dirIp']== None:
        return jsonify({'error': 'dirIP must be indicate'}), 400
    else:
        if posted_json['dirIp'].find('://') == -1:
            return jsonify({'error' : 'dirIP must contain the protocol, with ://'}), 400
        else:
            if posted_json['flujo_dashboard'] and posted_json['flujo_dashboard'].find('://') == -1:
                return jsonify({'error': 'flujo_dashboard must contain the protocol, with ://'}), 400
            else:
                camera= Camera(posted_json['dirIp'])
                camera.dirIp=(camera.dirIp).replace(" ", "")
                if camera.dirIp[len(camera.dirIp)-1] == '/':
                    camera.dirIp=camera.dirIp[:-1]
                print(camera.dirIp)

                # evaluate if exists this camera in DB yet
                session = Session()
                camera_evaluate=session.query(Camera).filter_by(dirIp=camera.dirIp).first()

                if not camera_evaluate:
                    #adding camera to the database
                    session.add(camera)
                    session.commit()
                    camera_evaluate = session.query(Camera).filter_by(dirIp=camera.dirIp).first()
                    session.close()

                id_camera = camera_evaluate.id
                id_admin = get_jwt_identity()

                print(id_admin)

                posted_cameraAdmin = CamerasAdminSchema(only=('user_camera', 'pass_camera', 'flujo_dashboard' ,'name')).load(posted_json)

                camera_admin = CamerasAdmin(id_admin, id_camera, **posted_cameraAdmin.data)

                camera_admin.flujo_dashboard= camera_admin.flujo_dashboard.replace(" ", "")

                print(camera_admin.id_admin)

                if len(session.query(CamerasAdmin).filter_by(id_admin=id_admin, id_camera=id_camera).all()) > 0:
                    session.close()
                    return jsonify({'error': 'this camera exist for this admin yet'}), 400

                else:
                    session.add(camera_admin)
                    session.commit()
                    session.close()
                    return jsonify(), 201


@app.route('/admin/camera/<id_camera>', methods=['PUT'])
@jwt_required
def edit_camera(id_camera):

    id_admin = get_jwt_identity()

    session = Session()
    camera_object = session.query(Camera).get(id_camera)
    if camera_object is None:
        user = session.query(Administrator).get(id_admin)
        session.close()
        return jsonify({'error': 'this camera does not exists'}), 400
    else:
        cameras_admin_object = session.query(CamerasAdmin).filter_by(id_admin=id_admin, id_camera=id_camera).all()
        if len(cameras_admin_object) <= 0:
            user = session.query(Administrator).get(id_admin)
            session.close()
            return jsonify({'error': 'this camera does not exists for the administrator {}'.format(user.username)}), 400
        else:
            posted_json = request.get_json()

            if posted_json['dirIp'] == None:
                session.close()
                return jsonify({'error': 'dirIP must be indicate'}), 400
            else:
                if posted_json['dirIp'].find('://') == -1:
                    session.close()
                    return jsonify({'error': 'dirIP must contain the protocol, with ://'}), 400
                else:
                    if posted_json['flujo_dashboard'] and posted_json['flujo_dashboard'].find('://') == -1:
                        session.close()
                        return jsonify({'error': 'flujo_dashboard must contain the protocol, with ://'}), 400
                    else:
                        camera= session.query(Camera).get(id_camera)
                        camera.dirIp=posted_json['dirIp']
                        camera.dirIp = (camera.dirIp).replace(" ", "")
                        if camera.dirIp[len(camera.dirIp) - 1] == '/':
                            camera.dirIp = camera.dirIp[:-1]

                        cameras_admin=session.query(CamerasAdmin).filter_by(id_camera=id_camera, id_admin=id_admin).all()
                        for camera_admin in cameras_admin:
                            camera_admin.user_camera=posted_json['user_camera']
                            camera_admin.pass_camera = posted_json['pass_camera']
                            camera_admin.flujo_dashboard = posted_json['flujo_dashboard'].replace(" ", "")
                            camera_admin.name = posted_json['name']

                        # updating exists cameras's instances in DB Camera and CamerasAdmin tables
                        session.commit()
                        session.close()
                        return jsonify(posted_json), 200


#Crear endpoint para eliminar una camara (de un administrador ¿y, si no queda referenciada por nadie ya, de to'do?).
@app.route('/admin/camera/<id_camera>', methods=['DELETE'])
@jwt_required
def delete_camera(id_camera):

    id_admin = get_jwt_identity()

    session = Session()
    camera_admin_object = session.query(CamerasAdmin).filter_by(id_admin=id_admin, id_camera=id_camera).all()
    if len(camera_admin_object) <= 0:
        user= session.query(Administrator).get(id_admin)
        session.close()
        return jsonify({'error': 'this camera does not exists for the administrator {}'.format(user.username)}), 400
    else:
        try:
            for element in camera_admin_object:
                session.delete(element)
            camera_admin_object = session.query(CamerasAdmin).filter_by(id_admin=id_admin, id_camera=id_camera).all()
            session.commit()
            session.close()
            if len(camera_admin_object) == 0:
                return jsonify(), 200
            else:
                return jsonify({'error': 'something was wrong in the proccess of delete'}), 500
        except Exception:
            session.close()
            return jsonify({'error': 'something was wrong in the proccess of delete'}), 500



@app.route('/accessLevels', methods=['GET'])
@jwt_required
def get_accessLevels():
    # fetching from the database
    session = Session()
    try:
        accessLevels_objects = session.query(AccessLevel).order_by(AccessLevel.id.asc()).all()
        accessLevels=[]
        session.close()
        for accessLevel in accessLevels_objects:
            accessLevels.append(accessLevel.alias)
        return jsonify({'accessLevels': accessLevels}), 200
    except Exception:
        return jsonify({'error': 'something was wrong in the proccess of getting access levels'}), 500


def update_max_level_schedules(maxlevel):
    session = Session()
    schedules_objects = session.query(Schedule).filter(Schedule.access_level_min>maxlevel).all()
    for schedule in schedules_objects:
        schedule.access_level_min=maxlevel
    session.commit()
    session.close()


@app.route('/accessLevels', methods=['POST'])
@jwt_required
def update_accessLevels():

    accessLevels = request.get_json()['accessLevels']
    # persist exam
    session = Session()
    #save old accessLevels
    old_accessLevels=session.query(AccessLevel).all()
    try:
        #Delete all
        session.query(AccessLevel).delete()
        for x, accessLevel in enumerate(accessLevels):
            element= AccessLevel(id=x, alias=accessLevel)
            session.add(element)
        session.commit()
        accessLevels_objects = session.query(AccessLevel).order_by(AccessLevel.id.asc()).all()
        accessLevels=[]
        session.close()
        for accessLevel in accessLevels_objects:
            accessLevels.append(accessLevel.alias)
        update_max_level_schedules(len(accessLevels) - 1)
        return jsonify({'accessLevels': accessLevels}), 200

    except Exception:
        print("ERROR: Access Levels aren't updated.")
        for old_access in old_accessLevels:
            session.add(old_access)
        session.close()
        return jsonify({'error': 'something was wrong in the proccess of updating access levels'}), 500





@app.route('/camera/<id_camera>/schedules', methods=['GET'])
@jwt_required
def get_schedules(id_camera):

    id_admin = get_jwt_identity()

    # fetching from the database
    session = Session()
    cameras_objects = session.query(CamerasAdmin).filter_by(id_camera=id_camera, id_admin=id_admin).all()
    if len(cameras_objects) == 0:
        session.close()
        return jsonify({ 'error': "this administrator doesn't have this camera asociated" }), 400
    else:
        schedules_objects = session.query(Schedule).filter_by(id_camera=id_camera).order_by(Schedule.start_time.asc()).all()
        schema= ScheduleSchema(many=True)
        # transforming into JSON-serializable objects
        schedules = schema.dump(schedules_objects)
        # serializing as JSON
        session.close()
        if len(schedules) == 0:
            return jsonify(schedules.data), 204
        else:
            return jsonify(schedules.data), 200


@app.route('/camera/<id_camera>/schedules', methods=['POST'])
@jwt_required
def add_schedule(id_camera):

    id_admin = get_jwt_identity()

    # fetching from the database
    session = Session()
    cameras_objects = session.query(CamerasAdmin).filter_by(id_camera=id_camera, id_admin=id_admin).all()
    if len(cameras_objects) == 0:
        session.close()
        return jsonify({ 'error': "this administrator doesn't have this camera asociated" }), 400
    else:
        if str(request.get_json()['id_camera']) != str(id_camera):
            session.close()
            return jsonify({'error': "the camera id of body must be the same of id_camera param"}), 400
        else:
            schedule_objects = session.query(Schedule).filter_by(id_camera=id_camera, start_time=request.get_json()['start_time']).all()
            if len(schedule_objects) > 0:
                session.close()
                return jsonify({'error': "Exists other schedule with the same start time already"}), 400
            else:
                # mount schedule object
                posted_schedule = ScheduleSchema(only=('id_camera',
                                                'start_time',
                                                'end_time',
                                                'access_level_min')).load(request.get_json())

                schedule = Schedule(**posted_schedule.data)

                # persist exam
                session.add(schedule)
                session.commit()

                # return created exam
                new_schedule = ScheduleSchema().dump(schedule).data
                session.close()
                return jsonify(new_schedule), 201



@app.route('/camera/<id_camera>/schedules/<id_schedule>', methods=['PUT'])
@jwt_required
def update_schedule(id_camera, id_schedule):
    # print(id_schedule)
    #
    # if len(str(id_schedule)) != 6:
    #     return jsonify({ 'error': "id_schedule must be the format HHMMSS" }), 400
    #
    # #converting id_schedule to a format like time (for example 034000 to 03:40:00)
    # hour=str(id_schedule)[0:2]
    # min=str(id_schedule)[2:4]
    # sec=str(id_schedule)[4:6]
    #
    # start_time=str(hour)+':'+str(min)+':'+str(sec)
    start_time=id_schedule
    print(start_time)

    id_admin = get_jwt_identity()

    # fetching from the database
    session = Session()
    cameras_objects = session.query(CamerasAdmin).filter_by(id_camera=id_camera, id_admin=id_admin).all()
    if len(cameras_objects) == 0:
        session.close()
        return jsonify({ 'error': "this administrator doesn't have this camera asociated" }), 400
    else:
        posted_json=request.get_json()
        if str(posted_json['id_camera']) != str(id_camera):
            session.close()
            return jsonify({'error': "the camera id of body must be the same of id_camera param"}), 400
        else:

            schedule_objects = session.query(Schedule).filter_by(id_camera=id_camera, start_time=request.get_json()['start_time']).all()
            if len(schedule_objects) > 0 and str(request.get_json()['start_time'])!= str(start_time):
                session.close()
                return jsonify({'error': "Exists other schedule with the same start time already"}), 400
            else:

                schedules = session.query(Schedule).filter_by(id_camera=id_camera, start_time=start_time).all()
                for schedule in schedules:
                    schedule.start_time=posted_json['start_time']
                    schedule.end_time=posted_json['end_time']
                    schedule.access_level_min=posted_json['access_level_min']
                # updating exists schedules's instances in DB Schedule tables
                session.commit()
                schedule = session.query(Schedule).filter_by(id_camera=id_camera, start_time=posted_json['start_time']).first()
                new_schedule= ScheduleSchema().dump(schedule).data
                session.close()

                return jsonify(new_schedule), 200


@app.route('/camera/<id_camera>/schedules/<id_schedule>', methods=['DELETE'])
@jwt_required
def delete_schedule(id_camera, id_schedule):
    # print(id_schedule)
    #
    # if len(str(id_schedule)) != 6:
    #     return jsonify({ 'error': "id_schedule must be the format HHMMSS" }), 400
    #
    # #converting id_schedule to a format like time (for example 034000 to 03:40:00)
    # hour=str(id_schedule)[0:2]
    # min=str(id_schedule)[2:4]
    # sec=str(id_schedule)[4:6]
    #
    # start_time=str(hour)+':'+str(min)+':'+str(sec)
    start_time=id_schedule
    print(start_time)

    id_admin = get_jwt_identity()

    # fetching from the database
    session = Session()
    cameras_objects = session.query(CamerasAdmin).filter_by(id_camera=id_camera, id_admin=id_admin).all()
    if len(cameras_objects) == 0:
        session.close()
        return jsonify({ 'error': "this administrator doesn't have this camera asociated" }), 400
    else:
        schedule_objects = session.query(Schedule).filter_by(id_camera=id_camera, start_time=start_time).all()
        if len(schedule_objects) == 0:
            session.close()
            return jsonify({'error': "Not exists a schedule with this start time"}), 400
        else:
            try:
                for element in schedule_objects:
                    session.delete(element)

                schedule_objects = session.query(Schedule).filter_by(id_camera=id_camera, start_time=start_time).all()
                session.commit()
                session.close()
                if len(schedule_objects) == 0:
                    return jsonify(), 200
                else:
                    return jsonify({'error': 'something was wrong in the proccess of delete'}), 500

            except Exception:
                session.close()
                return jsonify({'error': 'something was wrong in the proccess of delete'}), 500



@app.route('/registry/<number>', methods=['GET'])
@jwt_required
def get_last_entries(number):
    # fetching from the database
    number=int(number)
    session = Session()
    entries_objects = session.query(EntriesRegistry).order_by(EntriesRegistry.timestmp.desc()).all()
    if len(entries_objects) > number :
        entries_objects = entries_objects[:number]
    entries = EntriesRegistrySchema(many=True).dump(entries_objects).data
    session.close()
    if len(entries_objects) == 0:
        return jsonify(), 204
    else:
        return jsonify(entries), 200


@app.route('/registry', methods=['GET'])
@jwt_required
def get_entries():
    # fetching from the database
    session = Session()
    entries_objects = session.query(EntriesRegistry).order_by(EntriesRegistry.timestmp.desc()).all()
    entries = EntriesRegistrySchema(many=True).dump(entries_objects).data
    session.close()
    if len(entries_objects) == 0:
        return jsonify(), 204
    else:
        return jsonify(entries), 200


@app.route('/registry/<number>/me', methods=['GET'])
@jwt_required
def get_last_entries_for_me(number):
    # fetching from the database
    number=int(number)

    id_admin = get_jwt_identity()

    session = Session()
    cameras_objects = session.query(CamerasAdmin).filter_by(id_admin=id_admin).all()
    id_cameras=[]
    for camera in cameras_objects:
        id_cameras.append(camera.id_camera)

    entries_objects = session.query(EntriesRegistry).filter(EntriesRegistry.id_camera.in_(id_cameras))\
        .order_by(EntriesRegistry.timestmp.desc()).all()
    if len(entries_objects) > number :
        entries_objects = entries_objects[:number]
    entries = EntriesRegistrySchema(many=True).dump(entries_objects).data
    session.close()
    if len(entries_objects) == 0:
        return jsonify(), 204
    else:
        return jsonify(entries), 200


@app.route('/registry/me', methods=['GET'])
@jwt_required
def get_entries_for_me():

    id_admin = get_jwt_identity()

    # fetching from the database
    session = Session()

    cameras_objects = session.query(CamerasAdmin).filter_by(id_admin=id_admin).all()

    id_cameras=[]
    for camera in cameras_objects:
        id_cameras.append(camera.id_camera)

    entries_objects = session.query(EntriesRegistry).filter(EntriesRegistry.id_camera.in_(id_cameras))\
        .order_by(EntriesRegistry.timestmp.desc()).all()

    entries = EntriesRegistrySchema(many=True).dump(entries_objects).data

    session.close()

    if len(entries_objects) == 0:
        return jsonify(), 204
    else:
        return jsonify(entries), 200


@app.route('/camera/<id_camera>/registry/<number>', methods=['GET'])
@jwt_required
def get_last_entries_for_camera(id_camera, number):
    # fetching from the database
    number=int(number)
    session = Session()
    entries_objects = session.query(EntriesRegistry).filter_by(id_camera=id_camera)\
        .order_by(EntriesRegistry.timestmp.desc()).all()
    if len(entries_objects) > number :
        entries_objects = entries_objects[:number]
    entries = EntriesRegistrySchema(many=True).dump(entries_objects).data
    session.close()
    if len(entries_objects) == 0:
        return jsonify(), 204
    else:
        return jsonify(entries), 200


@app.route('/camera/<id_camera>/registry', methods=['GET'])
@jwt_required
def get_entries_for_camera(id_camera):
    # fetching from the database
    session = Session()
    entries_objects = session.query(EntriesRegistry).filter_by(id_camera=id_camera)\
        .order_by(EntriesRegistry.timestmp.desc()).all()
    entries = EntriesRegistrySchema(many=True).dump(entries_objects).data
    session.close()
    if len(entries_objects) == 0:
        return jsonify(), 204
    else:
        return jsonify(entries), 200


@app.route('/registry', methods=['POST'])
def add_entry():
    # mount entry object
    posted_entry = EntriesRegistrySchema(only=('id_camera', 'names', 'image_b64')).load(request.get_json())

    ts = time()
    timestamp = datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
    entry = EntriesRegistry(**posted_entry.data)
    entry.timestmp=timestamp

    # persist exam
    session = Session()
    session.add(entry)
    session.commit()

    # return created exam
    new_entry = EntriesRegistrySchema().dump(entry).data
    session.close()
    return jsonify(new_entry), 201




def create_config_file_sm(content, camera, cameraAdmin):

    script_path = str(configurations['PATH_SM'])

    # port_entrada=28800+int(camera.id)
    # port_salida=29800+int(camera.id)
    port_entrada=content["port_entrada"]
    port_salida=content["port_salida"]
    host= configurations["HOST"]

    actuador_entrada=content["actuador_entrada"]
    actuador_salida=content["actuador_salida"]
    push_contador=content["push_contador"]
    print(push_contador)

    content="""#Valores necesarios: HOST_API_BACKEND, PORT_API_BACKEND, HOST, PORT_ENTRADA, PORT_SALIDA, URL_CAMARA, ACTUADOR, PUSH_CONTADOR


#HOST y PORT del backend, donde se creará una entrada en el registro
HOST_BACKEND="""+str(configurations['HOST'])+"""
PORT_BACKEND="""+str(configurations['PORT'])+"""

#URL del flujo de video de la cámara que actuará con esta máquina de estados
URL_CAMARA="""+str(camera.dirIp)+"""
#ID de la camara
ID_CAMERA="""+str(camera.id)+"""

#Dirección donde se encuentra el detector de personas (no el reconocedor)
YOLO_ADDRESS="""+str(configurations['YOLO_ADDRESS'])+"""

#Dirección donde se encuentra el reconocedor facial
REC_ADDRESS="""+str(configurations['REC_ADDRESS'])+"""

#Usuario y contraseña de la camara
USUARIO="""+str(cameraAdmin.user_camera)+"""
PASSWORD="""+str(cameraAdmin.pass_camera)+"""

#Valor del Host donde se ejecuta este script
HOST="""+str(host)+"""
#Valor del puerto donde se reciben notificaciones de entrada
PORT_ENTRADA="""+str(port_entrada)+"""
#Valor del puerto donde se reciben notificaciones de entrada
PORT_SALIDA="""+str(port_salida)+"""


#URL del actuador que se ejecuta cuando alguien entra (en blanco no ejecutará actuador)
ACTUADOR_ENTRADA="""+str(actuador_entrada)+"""
#URL del actuador que se ejecuta cuando alguien sale (en blanco no ejecutará actuador)
ACTUADOR_SALIDA="""+str(actuador_salida)+"""
#URL donde se envía el valor del contador de personas (en blanco no hará un push del contador)
#Donde se coloque el [CONTADOR], será donde viaje el valor del contador en la URL
PUSH_CONTADOR="""+str(push_contador)+"""
"""


    system("echo \""+content+"\" > "+script_path+"/settings"+str(camera.id)+".conf")

    return port_entrada,port_salida,host

@app.route('/statemachine/<id_camera>/run', methods=['POST'])
@jwt_required
def run_state_machine(id_camera):

    posted_entry = request.get_json()

    id_admin = get_jwt_identity()

    session = Session()

    camera=session.query(Camera).filter_by(id=id_camera).first()
    cameraAdmin=session.query(CamerasAdmin).filter_by(id_camera=id_camera, id_admin=id_admin).first()

    #creamos fichero de configuración en el path de la maquina de estados, con la información que se nos envía en el cuerpo de la petición
    port_entrada, port_salida, host = create_config_file_sm(posted_entry, camera, cameraAdmin)

    #lanzamos la máquina de estados en un nuevo proceso, y guardamos su PID
    path = str(configurations['PATH_SM'])
    script_path = path.__add__("/Door.py")
    output_path = path.__add__("/output.txt")
    config_path = path.__add__("/settings"+str(camera.id)+".conf")

    # try:
    #pid=system("python3.6 "+script_path+" > "+output_path+" &")
    # proc = Popen('python3.6 '+ script_path + " > "+output_path+" &")
    with open(output_path, "w+") as output:
        proc=Popen(["/usr/bin/python3", script_path, config_path, path], stdout=output)
        pid=proc.pid

    # almacenamos el estado de la máquina en la tabla de máquinas activas
    entry = StateMachine(pid, id_camera, id_admin, port_entrada, port_salida, host)
    session.add(entry)
    session.commit()

    # devolvemos mensaje indicando que se ha lanzado la máquina
    session.close()
    return jsonify({"status":"running",
                    "port_entrada": port_entrada,
                    "port_salida": port_salida,
                    "host": host}), 201
    # except Exception:
    #     session.close()
    #     return jsonify({"status":"failed"}), 500


@app.route('/statemachine/<id_camera>/status', methods=['GET'])
@jwt_required
def get_state_machine(id_camera):

    # comprobamos si existe una maquina de estados para la camara indicada
    session = Session()
    statemachines = session.query(StateMachine).filter_by(id_camera=id_camera).all()

    if len(statemachines) == 0:
        return jsonify({ "status": "not working"}), 204
    else:
        sm = statemachines[0]
        return jsonify({"status": "running",
                        "port_entrada": sm.port_entrada,
                        "port_salida": sm.port_salida,
                        "host" : sm.host}), 200


@app.route('/statemachine/<id_camera>/stop', methods=['GET'])
@jwt_required
def stop_state_machine(id_camera):

    # comprobamos si existe una maquina de estados para la camara indicada
    session = Session()
    statemachines = session.query(StateMachine).filter_by(id_camera=id_camera).all()

    for sm in statemachines:
        try:
            kill(sm.pid, SIGTERM)
            session.delete(sm)
        except ProcessLookupError:
            session.delete(sm)
    session.commit()

    statemachines = session.query(StateMachine).filter_by(id_camera=id_camera).all()

    if len(statemachines) == 0:
        return jsonify({ "status": "not working"}), 204
    else:
        return jsonify({"status": "running"}), 400

"""
    Tomar el puerto 28800+id_camara para entrada y 29800+id_camara para salida por defecto
        Si se modifican (touched), mostrar mensaje indicando que pueden producirse colisiones entre los puertos
        
    Tomar el HOST automáticamente
    
    Tomar el usuario y contraseña de la cámara automáticamente
    
    El  
"""