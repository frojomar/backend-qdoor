# coding=utf-8

from sqlalchemy import Column, SmallInteger, String, PrimaryKeyConstraint

from .entity import Entity, Base

from marshmallow import Schema, fields

class AccessLevel(Entity, Base):

    __tablename__ = 'AccessLevels'

    id = Column(SmallInteger, primary_key=True, autoincrement=False)
    alias = Column(String(50), nullable=False, unique= True)

    # __table_args__ = (
    #     PrimaryKeyConstraint('id'),
    #     {},
    # )

    def __init__(self, id, alias):
        Entity.__init__(self)
        self.id = id
        self.alias = alias


class AccessLevelSchema(Schema):
    id = fields.Number()
    alias = fields.Str()
    created_at = fields.DateTime()
    updated_at = fields.DateTime()
