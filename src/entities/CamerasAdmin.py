# coding=utf-8

from sqlalchemy import Column, Integer, String, PrimaryKeyConstraint, ForeignKeyConstraint

from .entity import Entity, Base

from marshmallow import Schema, fields

class CamerasAdmin(Entity, Base):

    __tablename__ = 'CamerasAdmin'

    id_admin = Column(Integer, autoincrement=False)
    id_camera = Column(Integer, autoincrement=False)
    user_camera = Column(String(50), nullable=True)
    pass_camera = Column(String(50), nullable=True)
    flujo_dashboard = Column(String(150), nullable=True)
    name = Column(String(100), nullable=False)

    __table_args__ = (
        PrimaryKeyConstraint('id_admin', 'id_camera'),
        ForeignKeyConstraint(['id_admin'], ['Administrator.id']),
        ForeignKeyConstraint(['id_camera'], ['Camera.id']),
        {},
    )

    def __init__(self, id_admin, id_camera, user_camera, pass_camera, flujo_dashboard, name):
        Entity.__init__(self)
        self.id_admin = id_admin
        self.id_camera = id_camera
        self.user_camera = user_camera
        self.pass_camera = pass_camera
        self.flujo_dashboard = flujo_dashboard
        self.name = name


class CamerasAdminSchema(Schema):
    id_admin = fields.Number()
    id_camera= fields.Number()
    user_camera = fields.Str()
    pass_camera = fields.Str()
    flujo_dashboard = fields.Str()
    name = fields.Str()
    created_at = fields.DateTime()
    updated_at = fields.DateTime()
