# coding=utf-8

from sqlalchemy import Column, Integer, String, Time, PrimaryKeyConstraint, ForeignKeyConstraint
from sqlalchemy.dialects.mysql import LONGTEXT, TIMESTAMP

from .entity import Entity, Base

from marshmallow import Schema, fields

class EntriesRegistry(Entity, Base):

    __tablename__ = 'EntriesRegistry'

    id_camera = Column(Integer)
    timestmp = Column(TIMESTAMP)
    names = Column(LONGTEXT)
    image_b64= Column(LONGTEXT)

    __table_args__ = (
        PrimaryKeyConstraint('id_camera', 'timestmp'),
        ForeignKeyConstraint(['id_camera'], ['Camera.id']),
        {},
    )

    def __init__(self, id_camera, names, image_b64):
        Entity.__init__(self)
        self.id_camera=id_camera
        self.names = names
        self.image_b64 = image_b64


class EntriesRegistrySchema(Schema):
    id_camera= fields.Number()
    timestmp = fields.Time()
    names = fields.Str()
    image_b64 = fields.Str()
    created_at = fields.DateTime()
    updated_at = fields.DateTime()
