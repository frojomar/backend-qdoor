# coding=utf-8

from datetime import datetime
from sqlalchemy import create_engine, Column, String, Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from ..readConfigurationsFile import loadConfigurations

configurations = {}
loadConfigurations('settings.conf', configurations)
print(configurations)

db_host = configurations['HOST_MARIA']
db_port = configurations['PORT_MARIA']
db_name = configurations['DATABASE_MARIA']
db_user = configurations['USER_MARIA']
db_password = configurations['PASS_MARIA']

engine = create_engine(f'mysql+pymysql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}', echo=False)
#engine = create_engine(f'postgresql://{db_user}:{db_password}@{db_url}/{db_name}')
#engine = create_engine('sqlite:///db.sqlite3')
Session = sessionmaker(bind=engine)

Base = declarative_base()


class Entity():

    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    def __init__(self):
        self.created_at = datetime.now()
        self.updated_at = datetime.now()

