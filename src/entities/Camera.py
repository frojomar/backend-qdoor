# coding=utf-8

from sqlalchemy import Column, Integer, String, PrimaryKeyConstraint

from .entity import Entity, Base

from marshmallow import Schema, fields

class Camera(Entity, Base):

    __tablename__ = 'Camera'

    id = Column(Integer, primary_key=True, autoincrement=True)
    dirIp = Column(String(150), nullable=False, unique= True)

    # __table_args__ = (
    #     PrimaryKeyConstraint('id'),
    #     {},
    # )

    def __init__(self, dirIp):
        Entity.__init__(self)
        self.dirIp = dirIp


class CameraSchema(Schema):
    id = fields.Number()
    dirIp = fields.Str()
    created_at = fields.DateTime()
    updated_at = fields.DateTime()
