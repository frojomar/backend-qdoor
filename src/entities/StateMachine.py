# coding=utf-8

from sqlalchemy import Column, Integer, String, ForeignKeyConstraint

from .entity import Entity, Base

from marshmallow import Schema, fields

from passlib.hash import pbkdf2_sha256 as sha256

class StateMachine(Entity, Base):

    __tablename__ = 'StateMachine'

    pid = Column(Integer, primary_key=True, autoincrement=False, nullable=False)
    id_camera = Column(Integer, nullable=False, unique= True)
    init_by = Column(Integer, unique=False, nullable=False)
    port_entrada = Column(Integer, unique=False, nullable=False)
    port_salida = Column(Integer, unique=False, nullable=False)
    host = Column(String(150), nullable=False)

    __table_args__ = (
        ForeignKeyConstraint(['id_camera'], ['Camera.id']),
        ForeignKeyConstraint(['init_by'], ['Administrator.id']),
        {},
    )
    def __init__(self, pid, id_camera, init_by, port_entrada, port_salida, host):
        Entity.__init__(self)
        self.pid = pid
        self.id_camera = id_camera
        self.init_by = init_by
        self.port_entrada = port_entrada
        self.port_salida = port_salida
        self.host = host


class StateMachineSchema(Schema):
    pid = fields.Number()
    id_camera = fields.Number()
    init_by = fields.Number()
    port_entrada = fields.Number()
    port_salida = fields.Number()
    host = fields.Str()
