# coding=utf-8

from sqlalchemy import Column, Integer, String, PrimaryKeyConstraint

from .entity import Entity, Base

from marshmallow import Schema, fields

from passlib.hash import pbkdf2_sha256 as sha256

class Administrator(Entity, Base):

    __tablename__ = 'Administrator'

    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String(50), nullable=False, unique= True)
    password = Column(String(50), nullable=False, unique= True)
    name = Column(String(100), nullable=False)

    # __table_args__ = (
    #     PrimaryKeyConstraint('id'),
    #     {},
    # )

    def __init__(self, username, password, name):
        Entity.__init__(self)
        self.username = username
        self.password = password
        self.name = name

    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)

    @staticmethod
    def verify_hash(password, hash):
        return sha256.verify(password, hash)


class AdministratorSchema(Schema):
    id = fields.Number()
    username = fields.Str()
    password = fields.Str()
    name = fields.Str()
    created_at = fields.DateTime()
    updated_at = fields.DateTime()
