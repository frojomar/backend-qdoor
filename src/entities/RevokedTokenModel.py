# coding=utf-8

from sqlalchemy import Column, Integer, String, Time, PrimaryKeyConstraint, ForeignKeyConstraint
from sqlalchemy.dialects.mysql import LONGTEXT, TIMESTAMP

from .entity import Entity, Base

from marshmallow import Schema, fields


class RevokedTokenModel(Entity, Base):
    __tablename__ = 'revoked_tokens'
    id = Column(Integer, primary_key=True)
    jti = Column(String(120))
