# coding=utf-8

from sqlalchemy import Column, SmallInteger, Integer, String, Time, PrimaryKeyConstraint, ForeignKeyConstraint

from .entity import Entity, Base

from marshmallow import Schema, fields

class Schedule(Entity, Base):

    __tablename__ = 'Schedule'

    id_camera = Column(Integer)
    start_time = Column(Time)
    end_time = Column(Time)
    access_level_min = Column(SmallInteger, nullable=False)

    __table_args__ = (
        PrimaryKeyConstraint('id_camera', 'start_time'),
        ForeignKeyConstraint(['id_camera'], ['Camera.id']),
        {},
    )

    def __init__(self, id_camera, start_time, end_time, access_level_min):
        Entity.__init__(self)
        self.id_camera=id_camera
        self.start_time = start_time
        self.end_time = end_time
        self.access_level_min = access_level_min


class ScheduleSchema(Schema):
    id_camera= fields.Number()
    start_time = fields.Time()
    end_time = fields.Time()
    access_level_min = fields.Number()
    created_at = fields.DateTime()
    updated_at = fields.DateTime()
