from flask_restful import Resource, reqparse
from src.entities.RevokedTokenModel import RevokedTokenModel

from src.entities.entity import Session, engine, Base
from src.entities.Administrator import Administrator

from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required,
                                get_jwt_identity, get_raw_jwt)

parser = reqparse.RequestParser()
parser.add_argument('username', help='This field cannot be blank', required=True)
parser.add_argument('password', help='This field cannot be blank', required=True)


class UserLogin(Resource):
    def post(self):
        data = parser.parse_args()
        session = Session()
        administrator = session.query(Administrator).filter_by(username=data['username']).first()
        session.close()

        if not administrator:
            return {'success': False,'message': 'User {} doesn\'t exist'.format(data['username'])}

        #TODO Añadir el tratamiento con HASH
        #if Administrator.verify_hash(data['password'], administrator.password):
        if data['password'] == administrator.password :
            access_token = create_access_token(identity=administrator.id)
            refresh_token = create_refresh_token(identity=administrator.id)
            return {
                'success': True,
                'message': 'Logged in as {}'.format(administrator.username),
                'access_token': access_token,
                'refresh_token': refresh_token
            }
        else:
            return {'success': False,'message': 'Wrong credentials'}


class UserLogoutAccess(Resource):
    @jwt_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti=jti)
            revoked_token.add()
            return {'message': 'Access token has been revoked'}
        except:
            return {'message': 'Something went wrong'}, 500


class UserLogoutRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti=jti)
            revoked_token.add()
            return {'message': 'Refresh token has been revoked'}
        except:
            return {'message': 'Something went wrong'}, 500


class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity=current_user)
        return {'access_token': access_token}


class SecretResource(Resource):
    @jwt_required
    def get(self):
        return {
            'answer': 42
        }